package com.kossovo.ci.reservations;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MicroReservationsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicroReservationsApplication.class, args);
	}

}
